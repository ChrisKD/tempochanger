const { exec } = require('child_process');
const fs = require('fs');

// var testament = "The New Testament"
var testament = "The Old Testament"
var ntOtFolder = `C:/Users/ckd16/Desktop/The Holy Bible - Word of Promise - Dramatized (NKJV) (MP3)/${testament}`;
folderArr = fs.readdirSync(ntOtFolder);

// for (let j = 0; j < 27; j++) { // New Testament
for (let j = 0; j < 39; j++) { // Old Testament
    var eachBook = folderArr[j];
    console.log(eachBook);
    var testamentChapter = `${testament}/${eachBook}`;
    var sourceFolder = `C:/Users/ckd16/Desktop/The Holy Bible - Word of Promise - Dramatized (NKJV) (MP3)/${testamentChapter}`;
    var outputFolder = `C:/Users/ckd16/Desktop/wop nkjv 1.7/${testamentChapter}`;
    var fileArr = fs.readdirSync(sourceFolder)

    for (let i = 0; i < fileArr.length; i++) {
        var fileName = fileArr[i];
        var inFileLoc = `${sourceFolder}/${fileName}`;
        var pitch = 1.0;
        var tempo = 1.7;
        var outFileLoc = `${outputFolder}/${fileName}`;
        exec(`ffmpeg -i "${inFileLoc}" -af asetrate=44100*${pitch},aresample=44100,atempo=${tempo} "${outFileLoc}"`);
        // break;
    }
}


