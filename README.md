# tempoChanger

This node.js project uses the sound library "ffmpeg" to speed up all the songs in a directory. In my specific use case, there were over 1000 sound tracks.

Instructions for PC users:

1. Download ffmpeg from https://www.videohelp.com/software/ffmpeg
2. Wherever you place the files, be sure to add that location to PATH in your environment variables
3. You need to go into the code and change the location of the files that you want to speed up, as well as the output folder. Additionally, it is recommended to only change the tempo of a few hundred songs at a time, instead of doing 1000+
4. Use PowerShell (important, as the code is designed to execute within PowerShell rather than command prompt)
5. > npm i
6. > node app.js